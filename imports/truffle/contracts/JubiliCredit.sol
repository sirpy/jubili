pragma solidity ^0.4.18;

import 'openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol';

contract JubiliCredit is MintableToken {
    string public name = "Jubili Credit";
    string public symbol = "JBLC";
    uint8 public decimals = 2;


    uint256 public constant INITIAL_SUPPLY = 0;

    /**
     * @dev Constructor that gives msg.sender all of existing tokens.
     */
    function JubiliCredit() public {
    }
}
