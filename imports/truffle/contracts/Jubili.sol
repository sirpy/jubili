pragma solidity ^0.4.18;

import './JubiliToken.sol';
import './JubiliCredit.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol';
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Jubili  {
  using SafeMath for uint256;

  struct User {
      bool gotBonus;
      address[] friends;
      uint totalStaking;
  }
  struct Friend {
    uint256 staking;
    string friendType;
    uint8 trust;
    bool isExistingFriend;//is existing friend
    bool isMutual;
  }

  uint public totalStaking = 0;
  uint public constant SOCIAL_TRUST_NEW = 2000;
  uint public constant SOCIAL_TRUST_FRIEND = 1000;
  uint public constant SOCIAL_TRUST_REPAID_PERCENTAGE = 10;

  mapping(address => User) public users;
  mapping(address => mapping(address => Friend)) public userFriends;

  JubiliToken public jblToken;
  JubiliCredit public jblCredit;

  event JubiliNewUser(address indexed user);
  event JubiliStakeFriend(address indexed from,address indexed to,uint amount, uint8 trustScore);

  function Jubili() public {
    totalStaking = 0;
    jblToken = new JubiliToken();
    jblCredit = new JubiliCredit();
  }
  function getFriends(address user) public constant returns (address[]) {
    return users[user].friends;
  }
  function isUser()  public constant returns (bool) {
    return users[msg.sender].gotBonus;
  }
  /* function getFriends() public constant returns (Friend[]) {
    Friend[] memory result = new Friend[](users[msg.sender].friends.length);
    mapping(address => Friend) friends = userFriends[msg.sender];
    for(uint x = 0; x < users[msg.sender].friends.length; x++) {
      address friendAddr = users[msg.sender].friends[x];
      result[x] = friends[friendAddr];
    }
    return result;
  } */
  function newUser() public {
    if(users[msg.sender].gotBonus)
      return;
    users[msg.sender].gotBonus = true ;
    jblToken.giveBonus(msg.sender,SOCIAL_TRUST_NEW,JubiliToken.TrustBonus.NewUser);
    emit JubiliNewUser(msg.sender);
  }

  function stakeFriend(address friend,uint amount,uint8 trust) public {
    require(jblToken.stakeFriend(msg.sender,friend,amount)==true);
    totalStaking.add(amount);
    User storage userRecord = users[msg.sender];
    Friend storage friendRecord = userFriends[msg.sender][friend];
    bool isExistingFriend = friendRecord.isExistingFriend;
    if(!isExistingFriend)
      userRecord.friends.push(friend);
    friendRecord.staking = friendRecord.staking.add(amount);
    friendRecord.trust = trust;
    friendRecord.isExistingFriend = true;
    emit JubiliStakeFriend(msg.sender,friend,amount,trust);
    //if new connection (opposite friendship exists but this one is new)
    if(isExistingFriend==false && userFriends[friend][msg.sender].isExistingFriend==true)
    {
      jblToken.giveBonus(msg.sender,SOCIAL_TRUST_FRIEND,JubiliToken.TrustBonus.NewFriend);
      jblToken.giveBonus(friend,SOCIAL_TRUST_FRIEND,JubiliToken.TrustBonus.NewFriend);
    }

  }

}
