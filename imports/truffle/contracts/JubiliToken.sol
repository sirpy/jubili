pragma solidity ^0.4.18;

import 'openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol';

contract JubiliToken is MintableToken {
    string public name = "Jubili Social Trust Token";
    string public symbol = "JBL";
    uint8 public decimals = 2;

    enum TrustBonus {NewUser,NewFriend,RepaidCredit}
    event SocialTrust(address indexed to,TrustBonus indexed reason, uint256 amount);

    uint256 public constant INITIAL_SUPPLY = 10000 * (10 ** uint256(decimals));

    /**
     * @dev Constructor that gives msg.sender all of existing tokens.
     */
    function JubiliToken() public {
      totalSupply_ = INITIAL_SUPPLY;
      balances[msg.sender] = INITIAL_SUPPLY;
      emit Transfer(0x0, msg.sender, INITIAL_SUPPLY);
    }


    /**
    * @dev Function to give trust tokens bonus
    * @param _to The address that will receive the trust tokens.
    * @param _amount The amount of tokens to mint.
    * @return A boolean that indicates if the operation was successful.
    */
   function giveBonus(address _to, uint256 _amount,TrustBonus _reason) onlyOwner canMint public returns (bool) {
     totalSupply_ = totalSupply_.add(_amount);
     balances[_to] = balances[_to].add(_amount);
     emit SocialTrust(_to, _reason,_amount);
     emit Transfer(address(0), _to, _amount);
     return true;
   }

   function stakeFriend(address _from,address _to, uint256 _amount) onlyOwner canMint public returns (bool) {
     require(balances[_from]>=_amount);
     balances[_from] = balances[_from].sub(_amount);
     //msg.sender=isOnlyOwner, add to the reserve balance
     balances[msg.sender] = balances[msg.sender].add(_amount);
     emit Transfer(_from,address(this), _amount);
     return true;
   }
}
