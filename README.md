1. install meteor
  - curl https://install.meteor.com/ | sh
2. install truffle
  - npm i -g truffle
3. clone the repo into a new directory
  - mkdir jubili
  - cd jubili
  - git clone https://gitlab.com/sirpy/jubili/
4. install npm modules
  - npm i
5. run truffle console+local eth node
  - cd imports/truffle
  - truffle develop
6. start meteor web server
  - meteor --settings settings.json
7. goto http://localhost:3000
