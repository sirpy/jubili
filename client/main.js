import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Switch, Route, Link, Redirect } from 'react-router-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SideBar from '/client/SideBar.jsx'
import JubiliAccount from '/client/JubiliAccount.jsx'
import Web3 from 'web3'

const Login = props => (
	<div>Hello</div>
)
const Main = props => (
	<div>Main</div>
)
const Layout = (props) => (
	<main style={{display:'flex'}}>
			<aside style={{display:'flex',flexDirection:'column'}}>
				<SideBar/>
			</aside>
			<div style={{display:'flex',flexDirection:'column'}}>
				{props.children}
			</div>
	</main>
)
const Routes = props => (
		<Layout>
			<Switch>
				<Route component={Login} exact path="/login" />
				<Route component={JubiliAccount} path="/account/:form" />
				<Redirect from="/" to="/account/newuser/"/>
			</Switch>
		</Layout>
) ;

const initWeb3 = async () => {
		if (typeof window.web3 === 'undefined') {
			// no web3, use fallback
			console.error("Please use a web3 browser");
		} else {
			// window.web3 == web3 most of the time. Don't override the provided,
			// web3, just wrap it in your Web3.
			web3 = new Web3(window.web3.currentProvider);

			// the default account doesn't seem to be persisted, copy it to our
			// new instance
			if(window.web3.eth.defaultAccount)
				web3.eth.defaultAccount = window.web3.eth.defaultAccount
			else {
				let accounts = await web3.eth.getAccounts()
				console.log("user accounts",accounts)
				web3.eth.defaultAccount = accounts[0]
			}
		}

}
Meteor.startup( async (f) => {
	await initWeb3()
	render(
		<MuiThemeProvider>
			<BrowserRouter>
		    <Routes />
		  </BrowserRouter>
		</MuiThemeProvider>
	  , document.getElementById('react-root')
	);
} );
