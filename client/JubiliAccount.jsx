//@flow
import React from "react"
import _ from 'lodash'
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import Web3 from 'web3'
import JubiliContract from '/imports/truffle/build/contracts/Jubili.json'
import JubiliTokenContract from '/imports/truffle/build/contracts/JubiliToken.json'
import Contract from 'truffle-contract'

import {
  Redirect
} from "react-router-dom";

type Props = {
};

type State = {

}
const styles = {
  block: {
    maxWidth: 250,
  },
  checkbox: {
    marginBottom: 16,
  },
  OptionsWrapper: {
    display:'flex',
    flexDirection:'column',
    justifyContent:'space-evenly',
  },
  FormTitle: {
    align:'center',
    fontSize:'2em',
    fontWeight:'bold'
  }
}
type Props = {
  match: {
    params: {
      form:string
    }
  }
};

type State = {
  isExistingUser:boolean
}

export default class JubiliAccount extends React.Component<Props,State> {
  constructor(props:Props) {
    super(props)
    this.state = {
      isExistingUser:false,
      form:props.match.params.form
    }
  }
  componentWillReceiveProps(nextProps) {
      this.setState({form:nextProps.match.params.form})
  }
  componentWillMount() {
    this.initcontracts()
  }
  async initcontracts() {

    this.Jubili = Contract(JubiliContract)
    this.JubiliToken = Contract(JubiliTokenContract)
    await this.Jubili.defaults({from:web3.eth.defaultAccount})
    await this.Jubili.setProvider(web3.currentProvider)
    await this.JubiliToken.defaults({from:web3.eth.defaultAccount})
    await this.JubiliToken.setProvider(web3.currentProvider)

    let contract = await this.Jubili.deployed()
    console.log("Started Jubili contract",contract)
    contract.allEvents({fromBlock:'latest'}).watch((err,res) => console.log(err,res))

    let tokenAddr = await contract.jblToken()
    this.JubiliToken = await this.JubiliToken.at(tokenAddr)
    console.log("JubiliToken",this.JubiliToken)
    let tokenContract = this.JubiliToken
    tokenContract.allEvents({fromBlock:'latest'}).watch((err,res) => console.log(err,res))

    let isExistingUser = await contract.isUser()
    console.log("newUser",isExistingUser)
    this.setState({isExistingUser})

  }
  updateValue(event) {
  }
  async submitNewUser()
  {
    console.log("calling new user",web3.eth.defaultAccount)
    let contract = await this.Jubili.deployed()
    let res = await contract.newUser()
    console.log("newuser",res)
    this.setState({isExistingUser:true,form:'ok'})

  }
  async submitStakeFriend()
  {
    try {
      console.log("calling stake friend",web3.eth.defaultAccount,parseInt(this.state.stake*100))
      let contract = await this.Jubili.deployed()
      let res = await contract.stakeFriend(this.state.friendAddr,parseInt(this.state.stake*100),this.state.trust)
      console.log("submitStakeFriend",res)
      this.setState({form:'ok'})

    } catch (e) {
      console.log("submitStakeFriend error",e)
    } finally {

    }

  }
  submit() {
    switch(this.state.form)
    {
      case 'newuser':
        this.submitNewUser()
        break;
      case 'stakefriend':
        this.submitStakeFriend()
        break;


    }
    return true
  }
  newUser() {
    if(this.state.isExistingUser)
        return (
          <div>You already have an account</div>
        )
    return (

      <div>
        <RaisedButton type="submit" label="Open Jubli Account" style={{marginTop:'20px'}}/>
      </div>
    )

  }
  stakeFriend() {
    return (
    <div>
      <div>
        <TextValidator
                  hintText="Friend address: 0x...."
                  onChange={(e,v) => this.setState({friendAddr:v})}
                  name="friendAddr"
                  value={this.state.friendAddr}
                  validators={['required', 'matchRegexp:^0x[0-9a-zA-Z]+$']}
                  errorMessages={["This field is required","Invalid addres format"]}
              />
      </div>
      <div>
        <TextValidator
          hintText="amount of Jubili Tokens"
          onChange={(e,v) => this.setState({stake:v})}
          name="stake"
          value={this.state.stake}
          validators={['required', 'matchRegexp:^[0-9]+(\.[0-9]{1,2})?$']}
          errorMessages={["This field is required","Invalid amount"]}
          />
      </div>
      <div>
        <TextValidator
          hintText="Trust Score 0-9"
          onChange={(e,v) => this.setState({trust:v})}
          name="trust"
          value={this.state.trust}
          validators={['required', 'matchRegexp:^[0-9]$']}
          errorMessages={["This field is required","Invalid Trust Score"]}
          />
      </div>
      <div>
        <RaisedButton type="submit" label="Stake Friend" style={{marginTop:'20px'}}/>
      </div>
    </div>
    )
  }
  step1() {
    return (
      <div>
        <div style={styles.FormTitle}>Create Jubili Account</div>
        <ValidatorForm
                onSubmit={() => this.submit()}
            >
          <div style={styles.OptionsWrapper}>
            <Checkbox
              label="משחקיה"
              checked={_.indexOf(this.state.locations,'משחקיה') >= 0}
              onCheck={() => this.updateCheck('משחקיה')}

              style={styles.checkbox}
              />
              <TextValidator
                        hintText="מספר טלפון נייד"
                        onChange={(e,v) => this.setState({mobile:v})}
                        name="mobile"
                        value={this.state.mobile}
                        validators={['required', 'matchRegexp:^[0-9]{3}\-[0-9]{7}$']}
                        errorMessages={['חובה למלא שדה זה', 'מספר נייד לא תקין']}
                        errorText = {this.state.locationError}
                    />
                  <RaisedButton type="submit" label="הרשם" style={{marginTop:'20px'}}/>
        </div>
      </ValidatorForm>
    </div>
    )
  }

  render() {
    console.log("rendering")
    var step;
    switch(this.state.form) {
      case 'newuser': step = this.newUser()
      break;
      case 'stakefriend': step = this.stakeFriend()
      break;
      default:
      case 'ok': step = <div>Request Successful!</div>
      break;
    }
    return (
      <div>
        <div>
          <ValidatorForm
                  onSubmit={() => this.submit()}
              >
            {step}
          </ValidatorForm>
        </div>
      </div>
    )
  }
}
